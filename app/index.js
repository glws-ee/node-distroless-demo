import Koa from 'koa';
import Router from '@koa/router';
import koaHelmet from 'koa-helmet';
import logger from 'koa-pino-logger';

const app = new Koa();
const router = new Router();

// Helmet needs to be used as early as possible to ensure the headers are set
app
  .use(koaHelmet())
  .use(logger());

router.get('/', ctx => {
  ctx.body = 'Hello World!';
});

app
  .use(router.routes())
  .use(router.allowedMethods())
  .listen(80);
