FROM node:14 AS build
WORKDIR /install
COPY package*.json ./
RUN npm install --production

FROM gcr.io/distroless/nodejs:14
WORKDIR /app
COPY . .
COPY --from=build /install/node_modules/ ./node_modules/
EXPOSE 80
CMD ["--require=@contrast/agent", "app/index.js"]
